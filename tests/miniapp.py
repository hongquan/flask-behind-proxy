from flask import Flask
from flask import redirect, url_for
from flask_behind_proxy import FlaskBehindProxy


def create_app(test_config=None):
    app = Flask(__name__)
    FlaskBehindProxy(app)

    @app.route('/')
    def hello_world():
        return redirect(url_for('redirected'))


    @app.route('/redirected')
    def redirected():
        return 'Redirected successfully'

    return app
