import pytest

from .miniapp import create_app


@pytest.fixture
def app():
    return create_app()


def test_app(client):
    r = client.get('/', headers={'X-Forwarded-Proto': 'https'})
    assert r.status_code == 302
    assert r.headers['Location'] == 'https://localhost/redirected'


def test_no_proxy(client):
    r = client.get('/')
    assert r.status_code == 302
    assert r.headers['Location'] == 'http://localhost/redirected'
